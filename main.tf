# Rancher resources

# Initialize Rancher server
resource "rancher2_bootstrap" "admin" {
  depends_on = [time_sleep.wait_60_seconds]

  provider = rancher2.bootstrap

  password  = var.admin_password
  telemetry = true
}

# Local resources

# Save kubeconfig file for interacting with the RKE cluster on your local machine
resource "local_file" "kube_config_server_yaml" {
  filename = format("%s/%s", path.root, "kube_config_server.yaml")
  content  = ssh_resource.retrieve_config.result
}

resource "ssh_resource" "retrieve_config" {
  host = var.node_public_ip
  commands = [
    "sudo sed \"s/127.0.0.1/${var.node_public_ip}/g\" /etc/rancher/k3s/k3s.yaml"
  ]
  user        = var.node_username
  private_key = var.ssh_private_key_pem
  depends_on  = [time_sleep.wait_30_seconds]
}

resource "time_sleep" "wait_30_seconds" {
  depends_on      = [var.rancher_server_dns]
  create_duration = "30s"
}

resource "time_sleep" "wait_60_seconds" {
  depends_on      = [helm_release.rancher_server]
  create_duration = "60s"
}

resource "ssh_resource" "scale_traefik" {
  host = var.node_public_ip
  commands = [
    "kubectl scale deploy traefik -n kube-system --replicas=3"
  ]
  user        = var.node_username
  private_key = var.ssh_private_key_pem
  depends_on  = [rancher2_bootstrap.admin]
}

# Install cert-manager helm chart
resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  chart            = "https://charts.jetstack.io/charts/cert-manager-v${var.cert_manager_version}.tgz"
  namespace        = "cert-manager"
  create_namespace = true
  wait             = true

  set {
    name  = "installCRDs"
    value = "true"
  }
}

# Install Rancher helm chart
resource "helm_release" "rancher_server" {
  depends_on = [
    helm_release.cert_manager,
  ]

  name             = "rancher"
  chart            = "https://releases.rancher.com/server-charts/latest/rancher-${var.rancher_version}.tgz"
  namespace        = "cattle-system"
  create_namespace = true
  wait             = true

  set {
    name  = "hostname"
    value = var.rancher_server_dns
  }

  set {
    name  = "replicas"
    value = 3
  }

  set {
    name  = "bootstrapPassword"
    value = var.rancher_bootstrap_password
  }
  set {
    name  = "ingress.tls.source"
    value = "letsEncrypt"
  }
  set {
    name  = "letsEncrypt.email"
    value = "devops@work4all.de"
  }
}
