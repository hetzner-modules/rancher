terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.5.1"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.24.0"
    }
    ssh = {
      source  = "loafoe/ssh"
      version = "2.2.0"
    }
  }
  required_version = ">= 1.0.0"
}

provider "helm" {
  kubernetes {
    config_path = local_file.kube_config_server_yaml.filename
  }
}

# Rancher2 bootstrapping provider
provider "rancher2" {
  alias = "bootstrap"

  api_url   = "https://${var.rancher_server_dns}"
  insecure  = true
  bootstrap = true
}
