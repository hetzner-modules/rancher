## [1.1.5](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.1.4...1.1.5) (2022-12-02)


### Bug Fixes

* removed k3s installation on server ([8be628a](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/8be628a233a3dbe4af93dcebbc31b812f804f35e))

## [1.1.4](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.1.3...1.1.4) (2022-12-02)


### Bug Fixes

* added rancher management replicas ([75a601d](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/75a601dc04955d0236f4cd3076e14c63bded404b))
* updated dependency ([3f20ded](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/3f20dedd1fcc2de9b1dad71688726c2f5b961dda))

## [1.1.3](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.1.2...1.1.3) (2022-12-02)


### Bug Fixes

* changed k3s installation script ([a1a4e92](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/a1a4e923d7349cea50befd81d43f52259ea20a8d))

## [1.1.2](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.1.1...1.1.2) (2022-12-02)


### Bug Fixes

* remove joined node ([276ca68](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/276ca68ac399202865d86b6aefe3a6803ea76673))

## [1.1.1](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.1.0...1.1.1) (2022-12-02)


### Bug Fixes

* added k3s multicluster installation ([6e5f14c](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/6e5f14c653022f6bc8663da36df9b062c84fbd6d))

# [1.1.0](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.0.3...1.1.0) (2022-12-02)


### Features

* added ha to control plane ([ea016d8](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/ea016d81d1eca4080f037217b0217e03beabe6f4))

## [1.0.3](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/compare/1.0.2...1.0.3) (2022-11-30)


### Bug Fixes

* changed tag_release branch ([ebb5d74](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-rancher/commit/ebb5d74e7a224d2046a8451648013f873a5dec2b))
