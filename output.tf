# Outputs

output "rancher_url" {
  value = "https://${var.rancher_server_dns}"
}

output "bootstrap_admin_token" {
  value = rancher2_bootstrap.admin.token
}